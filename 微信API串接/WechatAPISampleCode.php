<?php
	header("Content-Type:text/html; charset=utf-8");
	//取得特店的HashKey - 由整合支付平台後台取得
	$strPlatFormHashKey = "===HASHKEY===";
	$TransType = array(
        "退款" => "refund",
        "單筆訂單查詢" => "queryorder",
		"批量訂單查詢" => "queryorders",
		"對帳" => "queryrecon",
		"匯率查詢" => "exchangerate"
    );
	//產生交易資料
	$postData = array(
		"PlatFormId" => "===MERCHANTID===", //特店編號 - 必填
		"PlatFormHashKey" => $strPlatFormHashKey, //特店HashKey
		"Terminal" => "WEB01", //終端編號 批量訂單查詢、對帳、兌換匯率查詢 必填
		"OrderId" => "20200324155119001", //您的訂單編號 - 只允許半形英文字母或數字組合 - 必填
		"ReCheckId" => "", //整合平台訂單號 - 整合支付平台回傳訂單號，用於退款、查詢之類的API
		"Amount" => "1", //您的訂單總金額 - 必填
		"ClientIP" => $_SERVER['REMOTE_ADDR']== "::1"?"127.0.0.1":$_SERVER['REMOTE_ADDR'], //消費者IP位址 - 必填
		"APIType" => $TransType["單筆訂單查詢"], //API代碼 - 必填
		"Orders_type" => "", //回傳資料形式 批量訂單、對帳使用 TER : 回傳依終端編號為主訂單明細 ALL : 回傳特店所有訂單明細
		"Order_start_time" => "", //特店訂單交易時間起 批量訂單使用 格式yyyyMMddHHmmss
		"Order_end_time" => "", //特店訂單交易時間迄 批量訂單使用 格式yyyyMMddHHmmss
		"Recondate" => "", //對帳日期 對帳使用 格式yyyyMMdd
		"DataType" => "", //回傳資料形式 JSON : JSON格式 XML : XML格式
		/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
		'ProductDetail'  => '', //若有申請電子發票 若有申請電子發票退款金額小於訂單金額必填 
		'Reason'  => '', //作廢原因 若有申請電子發票退款金額小於訂單金額必填
	);
	
	$url = "===PAYSERVERAPIURL===";
	$str_Result = formPost($url,$postData);
	echo var_dump($str_Result);
	
	function generateHash($data) {
		$require = array(
			"PlatFormId","Terminal","OrderId","ReCheckId","Amount","APIType",
            "Orders_type","Order_start_time","Order_end_time","Recondate","DataType",
            "ProductDetail","Reason","ClientIP");

		$paras = array();
		foreach($require as $k=>$v) {
			if(!is_null($v) && $v!="")
			{
				$paras[$v] = ( isset($data[$v]) ? $data[$v] : '' ); 
			}
			
		}
		natcaseksort($paras);
		
		$HashKey = strtoupper(hash('sha256', $data['PlatFormHashKey'].urldecode(http_build_query($paras))));

		return $HashKey;
	}
	
	function formPost($url,$post_data){
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($post_data),
			)
		);

		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;
	}
	function natksort( &$array ) {
		uksort( $array, 'strnatcmp' );
	}
	function natcaseksort( &$array ) {
		uksort( $array, 'strnatcasecmp' );
	}
?>
