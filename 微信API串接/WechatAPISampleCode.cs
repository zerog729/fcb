﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace APITransSample
{
    public partial class APITransSample : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //取得特店的HashKey - 由整合支付平台後台取得
            const string strPlatFormHashKey = "===HASHKEY===";
            //產生交易資料
            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.put("PlatFormId", "===MERCHANTID==="); //特店編號 - 必填
			postData.put("PlatFormHashKey", strPlatFormHashKey); //特店HashKey
			postData.put("Terminal", "WEB01"); //終端編號 批量訂單查詢、對帳、兌換匯率查詢 必填
			postData.put("OrderId", "20200324155119001"); //您的訂單編號 - 只允許半形英文字母或數字組合 - 退款、單筆訂單查詢 必填
			postData.put("ReCheckId", ""); //整合平台訂單號 - 整合支付平台回傳訂單號，退款、單筆訂單查詢 必填
			postData.put("Amount", "1"); //您的訂單總金額 - 退款、單筆訂單查詢 必填
			postData.put("ClientIP", ipAddress); //消費者IP位址 - 必填
			postData.put("APIType", TransType.信用卡一般交易); //API代碼
			postData.put("Orders_type", ""); //回傳資料形式 批量訂單、對帳使用 TER : 回傳依終端編號為主訂單明細 ALL : 回傳特店所有訂單明細
			postData.put("Order_start_time", ""); // 特店訂單交易時間起 批量訂單使用 格式yyyyMMddHHmmss
			postData.put("Order_end_time", ""); //特店訂單交易時間迄 批量訂單使用 格式yyyyMMddHHmmss	
			postData.put("Recondate", ""); //對帳日期 對帳使用 格式yyyyMMdd
			postData.put("DataType", "JSON"); //回傳資料形式 JSON : JSON格式 XML : XML格式
			/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
			postData.put("ProductDetail",'');////若有申請電子發票 若有申請電子發票退款金額小於訂單金額必填 
			postData.put("Reason","");//作廢原因 若有申請電子發票退款金額小於訂單金額必填
			postData.put("HashKey", getHashSha256(postData)); //產生驗證金鑰
            //發送交易
            var str_Result = Post("===PAYSERVERAPIURL===", postData, Encoding.UTF8);
        }

        private string GetIPAddress()
        {
            HttpContext context = HttpContext.Current;
            string sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(sIPAddress))
            {
                if (context.Request.ServerVariables["REMOTE_ADDR"] == "::1")
                {
                    return "127.0.0.1";
                }
                else
                {
                    return context.Request.ServerVariables["REMOTE_ADDR"].Split(':')[0];
                }
            }
            else
            {
                string[] ipArray = sIPAddress.Split(new Char[] { ',' });
                return ipArray[0].Split(':')[0];
            }
        }

        private string getHashSha256(Dictionary<string, string> data)
        {
            List<string> required = new List<string> {
                "PlatFormId","Terminal","OrderId","ReCheckId","Amount","APIType",
				"Orders_type","Order_start_time","Order_end_time","Recondate","DataType",
				"ProductDetail","Reason","ClientIP"
            };
            required.Sort();
            Dictionary<string, string> req = new Dictionary<string, string>();
            foreach (string idx in required)
            {
				if(!string.IsNullOrEmpty(data[idx]))
					req.Add(idx, data.ContainsKey(idx) ? data[idx] : "");
            }
            string text = string.Join("&", req.Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));
            text = data["PlatFormHashKey"] + text;
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString.ToUpper();
        }

        private string Post(string Baseurl, Dictionary<string, string> obj, Encoding encode)
        {
            var source = obj;
            using (WebClient client = new WebClient())
            {
                if (Baseurl.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                {
                    ServicePointManager.ServerCertificateValidationCallback = ((object param0, X509Certificate param1, X509Chain param2, SslPolicyErrors param3) => true);
                }

                NameValueCollection nvc = new NameValueCollection();
                nvc = source.Aggregate(new NameValueCollection(),
                (seed, current) =>
                {
                    if (current.Value == null)
                    {
                        seed.Add(current.Key, null);
                    }
                    else
                    {
                        seed.Add(current.Key, current.Value.ToString());
                    }
                    return seed;
                });

                byte[] response =
                client.UploadValues(Baseurl, nvc);

                string result = encode.GetString(response);
                return result;
            }
        }
    }

    public enum TransType
    {
		退款 = "refund",
        單筆訂單查詢 = "queryorder",
		批量訂單查詢 = "queryorders",
		對帳 = "queryrecon",
		匯率查詢 = "exchangerate"
    }
}