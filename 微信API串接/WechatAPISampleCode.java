import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/WechatAPISampleCode")
public class APISampleCode extends HttpServlet {
	private HashMap<String, String> TransType = new HashMap<String, String>()
	{
	    {
	        put("退款", "refund");
			put("單筆訂單查詢", "queryorder");
			put("批量訂單查詢", "queryorders");
			put("對帳", "queryrecon");
			put("匯率查詢", "exchangerate");
	    }
	};

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8"); // 避免PrintWriter寫出中文時出現亂碼
		//get client ip
		String ipAddress="";
	  Set<String> localhostAddresses = new HashSet<String>();
	  localhostAddresses.add(InetAddress.getLocalHost().getHostAddress());
	  for (InetAddress address : InetAddress.getAllByName("localhost")) {
	    localhostAddresses.add(address.getHostAddress());
	  }
	   
	  if (localhostAddresses.contains(request.getRemoteAddr())) {
	   	ipAddress="127.0.0.1";
	  } else {
	   	ipAddress=request.getRemoteAddr();
	  }

		//取得特店的HashKey - 由整合支付平台後台取得
        String strPlatFormHashKey = "===HASHKEY===";
        //產生交易資料
        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("PlatFormId", "===MERCHANTID==="); //特店編號 - 必填
        postData.put("PlatFormHashKey", strPlatFormHashKey); //特店HashKey
        postData.put("Terminal", "WEB01"); //終端編號 批量訂單查詢、對帳、兌換匯率查詢 必填
        postData.put("OrderId", "20200324155119001"); //您的訂單編號 - 只允許半形英文字母或數字組合 - 退款、單筆訂單查詢 必填
        postData.put("ReCheckId", ""); //整合平台訂單號 - 整合支付平台回傳訂單號，退款、單筆訂單查詢 必填
        postData.put("Amount", "1"); //您的訂單總金額 - 退款、單筆訂單查詢 必填
		postData.put("ClientIP", ipAddress); //消費者IP位址 - 必填
        postData.put("APIType", TransType.get("單筆訂單查詢").toString()); //API代碼
        postData.put("Orders_type", ""); //回傳資料形式 批量訂單、對帳使用 TER : 回傳依終端編號為主訂單明細 ALL : 回傳特店所有訂單明細
        postData.put("Order_start_time", ""); // 特店訂單交易時間起 批量訂單使用 格式yyyyMMddHHmmss
		postData.put("Order_end_time", ""); //特店訂單交易時間迄 批量訂單使用 格式yyyyMMddHHmmss	
        postData.put("Recondate", ""); //對帳日期 對帳使用 格式yyyyMMdd
		postData.put("DataType", "JSON"); //回傳資料形式 JSON : JSON格式 XML : XML格式
		/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
		postData.put("ProductDetail",'');////若有申請電子發票 若有申請電子發票退款金額小於訂單金額必填 
		postData.put("Reason","");//作廢原因 若有申請電子發票退款金額小於訂單金額必填
        postData.put("HashKey", sha256(postData)); //產生驗證金鑰
        
        PrintWriter writer = response.getWriter();
        String url = "===PAYSERVERAPIURL===";
        writer.println(formPost(url,postData));
        
	}
	private String formPost(String request, HashMap<String, String> post_data) throws IOException {
		String urlParameters  = http_build_query(post_data);
		byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
		int    postDataLength = postData.length;
		URL    url            = new URL( request );
		HttpURLConnection conn= (HttpURLConnection) url.openConnection();           
		conn.setDoOutput( true );
		conn.setInstanceFollowRedirects( false );
		conn.setRequestMethod( "POST" );
		conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
		conn.setRequestProperty( "charset", "utf-8");
		conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
		conn.setUseCaches( false );
		 // Send POST output.
	    DataOutputStream printout = new DataOutputStream (conn.getOutputStream ());
	    printout.write (postData);
	    printout.flush ();
	    printout.close ();
	    // Get response data.
	    BufferedReader bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	    String line = "";
	    StringWriter writer = new StringWriter();
	    while((line=bufReader.readLine())!=null){
	        writer.write(line);
	    }
	    bufReader.close();
	    writer.close();
	    return writer.toString();
	}
	public String sha256(Map<String, String> data) {
        List<String> required = new ArrayList<String>(){{
            add("PlatFormId");
			add("Terminal");
            add("OrderId");
            add("ReCheckId");
            add("Amount");
            add("ClientIP");
            add("APIType");
            add("Orders_type");
            add("Order_start_time");
            add("Order_end_time");
            add("Recondate");
            add("DataType");
			add("ProductDetail");
			add("Reason");
        }};
        Collections.sort(required, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        StringBuilder sb = new StringBuilder();
        for(String idx : required) {
			if(data.get(idx) != null && !data.get(idx).isEmpty())
			{
				sb.append("&"+idx+"="+data.get(idx));
			}
				
        }
        String str = sb.toString();
        String base = data.get("PlatFormHashKey")+str.substring(1, str.length());
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
	private String http_build_query(Map<String, String> params) throws UnsupportedEncodingException {
		String internalEncoding = "UTF-8";
	    String result = "";
	    for(Map.Entry<String, String> e : params.entrySet()){
	        if(e.getKey().isEmpty()) continue;
	        if(!result.isEmpty()) result += "&";
	        result += URLEncoder.encode(e.getKey(), internalEncoding) + "=" + 
	                  URLEncoder.encode(e.getValue(), internalEncoding);
	    }
	    return result;
	}
}
