package sample;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.security.MessageDigest;


public final class Paypage extends HttpServlet {

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
      throws IOException, ServletException {

		//get client ip
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		//get now time
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		Date date = new Date();

		//get timezone
		TimeZone tz = TimeZone.getDefault();  
		Calendar cal = GregorianCalendar.getInstance(tz);
		int offsetInMillis = tz.getOffset(cal.getTimeInMillis());
		String offset = String.format("%02d%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = (offsetInMillis >= 0 ? "+" : "-") + offset;

		//API串接參數設定
		Map<String,String> map = new HashMap<String, String>();
		map.put("PlatFormId", "===MERCHANTID==="); //特店編號
		map.put("PlatFormHashKey", "===HASHKEY==="); //HASHKEY
		map.put("PayType", "ALL"); //ALL:整合支付平台支付頁面,EATM:eATM,IDP:活期帳戶,REG:ATM,CREDIT:信用卡,UNION:銀聯卡,CS : 四大超商,WECHAT : 微信,TWPAY:台灣PAY
		map.put("OrderId", ""); //您的訂單編號 - 必填
		map.put("Amount", ""); //您的訂單總金額 - 必填
		map.put("ShippingFee", "0");//運費 - (限數字格式)
		map.put("ProductName", "");//商品說明
		map.put("PayTitle", ""); //付款說明 - 必填
		map.put("OrderArea","");
		map.put("OrderAddress","");
		map.put("OrderReceiver","test");
		map.put("OrderEmail","test@hotmail.com");
		map.put("ClientIP", ipAddress); //消費者IP
		map.put("ResURL", ""); //您的接收支付訊息頁面
		map.put("TimeZone", offset); //交易時區 - 必填
		map.put("CreateTime", dateFormat.format(date)); //訂單建立時間
		map.put("TransTime", dateFormat.format(date)); //開炲交易時間
		/*  eATM專區設定  */
		map.put("InAccountNo",""); //銷帳編號, 使用eATM/ATM/活期帳戶/四大超商必填
		map.put("OutAccountNo","");//支出帳戶, 使用活期帳戶必填
		map.put("OutBank","");//支出銀行, 使用活期帳戶必填
		map.put("ID","");//身分字號, 使用活期帳戶必填
		map.put("FunCode","");//功能代碼
		map.put("Apply","");//是否申請銷帳編號:空白-不申請 Y-申請 N-取消N
		map.put("DueDate","");//銷百繳款期限, 使用活期帳戶必填
		/*  信用卡專區設定 */
		map.put("AutoCap","0"); //自動轉入請款檔 (0:不轉入, 1:自動轉入)
		map.put("TransType","1"); //信用卡-交易類型(1:一般, 2:分期, 3:紅利)
		map.put("PeriodNum","0"); //信用卡分期期數
		map.put("BonusActionCode",""); //信用卡紅利活動代碼
		map.put("TimeoutSecs", "600"); //信用卡-交易逾時秒數
		map.put("LagSelect","0"); //信用卡-預設顯示語系(0:繁中, 1:簡中, 2:English, 3:日本語)
		map.put("CustomResultPage", "1");//是否需要使信用顯示付款結果頁, 0:使用第一銀行預設模板顯示付款結果, 1:特店自訂結果頁面顯示付款資料 - 必填
		/*  微信專區設定  */
		map.put("Terminal","");//微信終端編號
		/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
		map.put("ProductDetail",'[{"Description":"紅茶","Quantity":"2","UnitPrice":"15","Amount":"30","Tax":"0","TaxType":"3"},{"Description":"綠茶","Quantity":"5","UnitPrice":"20","Amount":"100","Tax":"0","TaxType":"3"}]');////若有申請電子發票 必填 由各支付欄位產生的JSON字串
		map.put("Buyer_Identifier","");//買方統編
		map.put("DonateMark","");//捐贈註記 : 非捐贈發票 1: 捐贈發票
		map.put("CUSTOMEREMAIL","");//客戶接收電子發票信箱
		map.put("CarrierId1","");//客戶載具號碼
		map.put("NPOBAN","");//捐贈發票愛心碼
		map.put("Amount_TaxRate","");//稅率 5%時本欄位為 0.05
		/* 加密字串  */
		map.put("HashKey", this.sha256(map));

		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();


		//output form
		writer.println("<form name='form1' id='form1' action='===PAYSERVERURL===' method='post'>");
		for (Map.Entry<String, String> entry : map.entrySet()) {
				writer.println("<input type='hidden' name='" + entry.getKey() + "' value='" + entry.getValue() + "' />");
		}
		writer.println("</form>");
		writer.println("<script>document.forms['form1'].submit();</script>");
    }

    public String sha256(Map<String, String> data) {
        List<String> required = new ArrayList<String>(){{
            add("PlatFormId");
            add("PayType");
            add("OrderId");
            add("Amount");
            add("ShippingFee");
            add("ProductName");
			add("PayTitle");
			add("HeadquarterID");
			add("StatesID");
			add("BranchAndPlatformID");
			add("BusinessUnit");
			add("SubMerchantID");
            add("OrderArea");
            add("OrderAddress");
            add("OrderReceiver");
            add("OrderEmail");
            add("ClientIP");
            add("ResURL");
            add("TimeZone");
            add("CreateTime");
			add("TransTime");
            add("NextURL");
            add("InAccountNo");
			add("OutAccountNo");
			add("OutBank");
            add("ID");
            add("FunCode");
			add("Apply");
            add("DueDate");
            add("AutoCap");
            add("TransType");
            add("PeriodNum");
            add("BonusActionCode");
			add("TimeoutSecs");
			add("LagSelect");
            add("CustomResultPage");
			add("Terminal");
			add("ProductDetail");
			add("Buyer_Identifier");
			add("DonateMark");
			add("CUSTOMEREMAIL");
			add("CarrierId1");
			add("NPOBAN");
			add("Amount_TaxRate");
        }};
        Collections.sort(required, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < required.size(); j++) {
			if(data.get(idx) != null && !data.get(idx).isEmpty())
			{
				if(j > 0)
        		sb.append("&"+idx+"="+data.get(idx));
				else
        		sb.append(idx+"="+data.get(idx));
			}
        }
        
        String base = data.get("PlatFormHashKey")+sb.toString();
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}