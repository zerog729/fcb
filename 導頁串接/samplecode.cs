using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

public void Main()
{
    //取得Client IP
    System.Web.HttpContext context = System.Web.HttpContext.Current; 
    string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

    //取得時區
    TimeZone zone = TimeZone.CurrentTimeZone;
	TimeSpan offset = zone.GetUtcOffset(DateTime.Now);

	string dateFormat = "yyyy/MM/dd HH:mm:ss";
    Dictionary<string, string> postData = new Dictionary<string, string>();
    postData.Add("PlatFormId", "===MERCHANTID==="); //特店編號
    postData.Add("PlatFormHashKey", "===HASHKEY==="); //HASHKEY
	postData.Add("PayType", "ALL"); //ALL:整合支付平台支付頁面,EATM:eATM,IDP:活期帳戶,REG:ATM,CREDIT:信用卡,UNION:銀聯卡,CS : 四大超商,WECHAT : 微信,TWPAY:台灣PAY
	postData.Add("OrderId", ""); //您的訂單編號 - 必填
    postData.Add("Amount", ""); //您的訂單總金額 - 必填
	postData.Add("ShippingFee", "0");//運費 - (限數字格式)
    postData.Add("ProductName", "");//商品說明
	postData.Add("PayTitle", ""); //付款說明 - 必填
	postData.Add("OrderArea", "");
	postData.Add("OrderAddress", "");
	postData.Add("OrderReceiver", "test");
	postData.Add("OrderEmail", "test@hotmail.com");  
    postData.Add("ClientIP", ipAddress); //消費者IP
    postData.Add("ResURL", ""); //您的接收支付訊息頁面
    postData.Add("TimeZone", offset.ToString("hhmm")); //交易時區 - 必填
    postData.Add("CreateTime", DateTime.Now.ToString(dateFormat)); //訂單建立時間
    postData.Add("TransTime", DateTime.Now.ToString(dateFormat)); //開炲交易時間
	/*  eATM專區設定  */
    postData.Add("InAccountNo",""); //銷帳編號, 使用eATM/ATM/活期帳戶/四大超商必填
	postData.Add("OutAccountNo","");//支出帳戶, 使用活期帳戶必填
	postData.Add("OutBank","");//支出銀行, 使用活期帳戶必填
    postData.Add("ID","");//身分字號, 使用活期帳戶必填
    postData.Add("FunCode","");//功能代碼
    postData.Add("Apply","");//是否申請銷帳編號:空白-不申請 Y-申請 N-取消N
    postData.Add("DueDate","");//銷百繳款期限, 使用活期帳戶必填
    /*  信用卡專區設定 */
	postData.Add("AutoCap","0"); //自動轉入請款檔 (0:不轉入, 1:自動轉入)
	postData.Add("TransType","1"); //信用卡-交易類型(1:一般, 2:分期, 3:紅利)
    postData.Add("PeriodNum","0"); //信用卡分期期數
    postData.Add("BonusActionCode",""); //信用卡紅利活動代碼
    postData.Add("TimeoutSecs", "600"); //信用卡-交易逾時秒數
    postData.Add("LagSelect","0"); //信用卡-預設顯示語系(0:繁中, 1:簡中, 2:English, 3:日本語)
	postData.Add("CustomResultPage", "");//是否需要使信用顯示付款結果頁, 0:使用第一銀行預設模板顯示付款結果, 1:特店自訂結果頁面顯示付款資料 - 必填
    /*  微信專區設定  */
    postData.Add("Terminal", "");//微信終端編號
	/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
	postData.Add("ProductDetail", '[{"Description":"紅茶","Quantity":"2","UnitPrice":"15","Amount":"30","Tax":"0","TaxType":"3"},{"Description":"綠茶","Quantity":"5","UnitPrice":"20","Amount":"100","Tax":"0","TaxType":"3"}]');//若有申請電子發票 必填 由各支付欄位產生的JSON字串
	postData.Add("Buyer_Identifier", "");//買方統編
	postData.Add("DonateMark", "");//捐贈註記 : 非捐贈發票 1: 捐贈發票
	postData.Add("CUSTOMEREMAIL", "");//客戶接收電子發票信箱
	postData.Add("CarrierId1", "");//客戶載具號碼
	postData.Add("NPOBAN", "");//捐贈發票愛心碼
	postData.Add("Amount_TaxRate", "");//稅率 5%時本欄位為 0.05
	
    postData.Add("HashKey", getHashSha256(postData));

    //output form
    Response.Write("<form name='form1' id='form1' action='===PAYSERVERURL===' method='post'>");
    foreach (KeyValuePair<string, string> item in postData)
	{
	    Response.Write(string.Format("<input type='hidden' name='{0}' value='{1}' />", item.Key, item.Value));
	}
    Response.Write("</form>");
    Response.Write("<script>document.forms['form1'].submit();</script>");
}
public static string getHashSha256(Dictionary<string, string> data)
{
    List<string> required = new List<string> {
        "PlatFormId","PayType","OrderId","Amount","ShippingFee","ProductName",
        "PayTitle","HeadquarterID","StatesID","BranchAndPlatformID","BusinessUnit","SubMerchantID",
        "OrderArea","OrderAddress","OrderReceiver","OrderEmail","ClientIP",
        "ResURL","TimeZone","CreateTime","TransTime","NextURL","InAccountNo","OutAccountNo",
        "OutBank","ID","FunCode","Apply","DueDate","AutoCap","TransType","PeriodNum",
        "BonusActionCode","TimeoutSecs","LagSelect","CustomResultPage",
        "Terminal","ProductDetail", "Buyer_Identifier","DonateMark","CUSTOMEREMAIL","CarrierId1",
        "NPOBAN" ,"Amount_TaxRate"};
    required.Sort();
    Dictionary<string, string> req = new Dictionary<string, string>();
    foreach (string idx in required)
    {
		if(!string.IsNullOrEmpty(data[idx]))
			req.Add(idx, data.ContainsKey(idx) ? data[idx] : "" );
    }
    string text = string.Join("&", req.Select(kvp =>string.Format("{0}={1}", kvp.Key, kvp.Value)));
	text = data["PlatFormHashKey"] + text;
    byte[] bytes = Encoding.UTF8.GetBytes(text);
    SHA256Managed hashstring = new SHA256Managed();
    byte[] hash = hashstring.ComputeHash(bytes);
    string hashString = string.Empty;
    foreach (byte x in hash)
    {
        hashString += String.Format("{0:x2}", x);
    }
    return hashString.ToUpper();
}