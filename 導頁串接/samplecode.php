<?php
date_default_timezone_set('Asia/Taipei'); //時區設定請參閱 http://php.net/manual/en/timezones.php

$dateFormat = "Y/m/d H:i:s";
$postData = array(
	'PlatFormId' => '===MERCHANTID===', //特店編號
	'PlatFormHashKey' => '===HASHKEY===', //HASHKEY
	'PayType' => 'ALL',//ALL:整合支付平台支付頁面
					   //EATM:eATM
					   //IDP:活期帳戶
					   //REG:ATM
                       //CREDIT:信用卡
                       //UNION:銀聯卡
					   //CS : 四大超商
					   //WECHAT : 微信
					   //TWPAY:台灣PAY
	'OrderId' => '', //您的訂單編號 - 必填
	'Amount' => 0, //您的訂單總金額 - 必填
	'ShippingFee' => 0, //運費 - (限數字格式)
	'ProductName' => '', //商品說明
	'PayTitle' => '', //付款說明 - 必填
	'OrderArea' => '',
    'OrderAddress' => '',
    'OrderReceiver' => 'test',
    'OrderEmail' => 'test@hotmail.com',
	'ClientIP' => $_SERVER['REMOTE_ADDR'], //消費者IP
	'ResURL' => '', //您的接收支付訊息頁面
	'TimeZone' => date("O"), //交易時區 - 必填
	'CreateTime' => date($dateFormat), //訂單建立時間
	'TransTime' => date($dateFormat), //開炲交易時間
	 /*  eATM專區設定  */
	'InAccountNo' => '', //銷帳編號, 使用eATM/ATM/活期帳戶/四大超商必填
	'OutAccountNo' => '', //支出帳戶, 使用活期帳戶必填
	'OutBank' => '', //支出銀行, 使用活期帳戶必填
	'ID' => '', //身分字號, 使用活期帳戶必填
	'FunCode' => '', //功能代碼
	'Apply'=> '', //是否申請銷帳編號:空白-不申請 Y-申請 N-取消N
	'DueDate'=> '', //銷百繳款期限, 使用活期帳戶必填
	
	/*  信用卡專區設定 */
	'AutoCap' => '0', //自動轉入請款檔 (0:不轉入, 1:自動轉入)
	'TransType' => '1', //信用卡-交易類型(1:一般, 2:分期, 3:紅利)
	'PeriodNum' => 0, //信用卡分期期數
	'BonusActionCode' => '', //信用卡紅利活動代碼
	'TimeoutSecs' => '600', //信用卡-交易逾時秒數
	'LagSelect' => '0', //信用卡-預設顯示語系(0:繁中, 1:簡中, 2:English, 3:日本語)
	'CustomResultPage' => '1', //是否需要使信用顯示付款結果頁, 0:使用第一銀行預設模板顯示付款結果, 1:特店自訂結果頁面顯示付款資料 - 必填
	
	/*  微信專區設定  */
	'Terminal'  => '', //微信終端編號
	
	/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
	'ProductDetail'  => '[{"Description":"紅茶","Quantity":"2","UnitPrice":"15","Amount":"30","Tax":"0","TaxType":"3"},{"Description":"綠茶","Quantity":"5","UnitPrice":"20","Amount":"100","Tax":"0","TaxType":"3"}]', //若有申請電子發票 必填 由各支付欄位產生的JSON字串
	'Buyer_Identifier'  => '', //買方統編
	'DonateMark'  => '', //捐贈註記 : 非捐贈發票 1: 捐贈發票
	'CUSTOMEREMAIL'  => '', //客戶接收電子發票信箱
	'CarrierId1'  => '', //客戶載具號碼
	'NPOBAN'  => '', //捐贈發票愛心碼
	'Amount_TaxRate'  => '' //稅率 5%時本欄位為 0.05
);

$postData = generateHash($postData);

echo "<form name='form1' id='form1' action='===PAYSERVERURL===' method='post'>";
foreach($postData as $k=>$v) {
	echo "<input type='hidden' name='$k' value='$v' /> ";
}
echo "</form>";

function generateHash($data) {
	$require = array(
		"PlatFormId","PayType","OrderId","Amount","ShippingFee","ProductName",
        "PayTitle","HeadquarterID","StatesID","BranchAndPlatformID","BusinessUnit","SubMerchantID",
        "OrderArea","OrderAddress","OrderReceiver","OrderEmail","ClientIP",
        "ResURL","TimeZone","CreateTime","TransTime","NextURL","InAccountNo","OutAccountNo",
        "OutBank","ID","FunCode","Apply","DueDate","AutoCap","TransType","PeriodNum",
        "BonusActionCode","TimeoutSecs","LagSelect","CustomResultPage",
        "Terminal","ProductDetail", "Buyer_Identifier","DonateMark","CUSTOMEREMAIL","CarrierId1",
        "NPOBAN" ,"Amount_TaxRate");

	$paras = array();
	foreach($require as $k=>$v) {
		if(!is_null($v) && $v!="")
			$paras[$v] = $data[$v]; 
	}
  uksort( $paras, 'strnatcasecmp' );

	$HashKey = strtoupper(hash('sha256', $data['PlatFormHashKey'].http_build_query($paras)));
	$data['HashKey'] = $HashKey;

	return $data;
}
?>
<script>
document.forms["form1"].submit();
</script>