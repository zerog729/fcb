<?php
	header("Content-Type:text/html; charset=utf-8");
	date_default_timezone_set('Asia/Taipei'); //時區設定請參閱 http://php.net/manual/en/timezones.php
	//取得特店的HashKey - 由整合支付平台後台取得
	$strPlatFormHashKey = "===HASHKEY===";
	$TransType = array(
        "信用卡一般交易" => 1,
        "信用卡分期交易" => 2,
        "信用卡紅利折抵" => 3,
        "信用卡取消授權" => 5,
        "信用卡轉入請款檔" => 6,
        "信用卡轉出請款檔" => 7,
        "信用卡退貨" => 8,
        "信用卡取消退貨" => 9,
        "信用卡交易查詢" => 16,
        "信用卡結帳" => 19,
        "銀聯卡授權" => 13,
        "銀聯卡取消授權" => 14,
        "銀聯卡取消退貨" => 15,
        "銀聯卡交易查詢" => 17
    );
	
	//產生交易資料
	$postData = array(
		"PlatFormId" => "===MERCHANTID===", //特店編號 - 必填
		"PlatFormHashKey" => $strPlatFormHashKey, //特店HashKey
		"PayType" => "CREDIT", //支付別 - CREDIT:信用卡, UNION:銀聯卡 - 必填
		"OrderId" => "20991231000001", //您的訂單編號 - 只允許半形英文字母或數字組合 - 必填
		"ReCheckId" => "", //整合平台訂單號 - 整合支付平台回傳訂單號，用於退款、查詢之類的API
		"Amount" => "1", //您的訂單總金額 - 必填
		"PayTitle" => "付款說明", //付款說明 - 必填
		"ClientIP" => $_SERVER['REMOTE_ADDR']== "::1"?"127.0.0.1":$_SERVER['REMOTE_ADDR'], //消費者IP位址 - 必填
		"ResURL" => "", //回傳網址 - 資料回傳網址(保留欄位) - 必填
		"HeadquarterID" => "", //集團別 - 由商家自行定義，例如: Grand Holding
		"StatesID" => "", //區域或州界別 - 由商家自行定義，例如: TW
		"BranchID" => "", //平台通路或社群別 - 由商家自行定義，例如: ePay
		"BusinessUnit" => "", //業務單位別 - 由商家自行定義，例如: PCHOME_EC
		"SubMerchantID" => "", //次特店別 - 由商家自行定義，例如: 12345678
		"TimeZone" => date("O"), //時區 - 格式：+0800 - 必填
		"CreateTime" => date("Y-m-d H:i:s"), //訂單建立時間 - 格式：yyyy/MM/dd HH:mm:ss - 必填
		"TransTime" => date("Y-m-d H:i:s"), //發生交易時間 - 格式：yyyy/MM/dd HH:mm:ss - 必填
		"TransType" => $TransType["信用卡一般交易"], //交易類型 - 格式：yyyy/MM/dd HH:mm:ss - 必填
		"PeriodNum" => "", //分期交易之期數
		"CardId" => "", //信用卡卡號 - 必填
		"ExpiryDate" => "", //信用卡有效日期 - 格式YYMM - 必填
		"CVV" => "", //信用卡背面末三碼 - 必填
		/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
		'ProductDetail'  => '[{"Description":"紅茶","Quantity":"2","UnitPrice":"15","Amount":"30","Tax":"0","TaxType":"3"},{"Description":"綠茶","Quantity":"5","UnitPrice":"20","Amount":"100","Tax":"0","TaxType":"3"}]', //若有申請電子發票 必填 由各支付欄位產生的JSON字串
		'Buyer_Identifier'  => '', //買方統編
		'DonateMark'  => '', //捐贈註記 : 非捐贈發票 1: 捐贈發票
		'CUSTOMEREMAIL'  => '', //客戶接收電子發票信箱
		'CarrierId1'  => '', //客戶載具號碼
		'NPOBAN'  => '', //捐贈發票愛心碼
		'Amount_TaxRate'  => '', //稅率 5%時本欄位為 0.05
		'Reason'  => '', //作廢原因
	);
	$postData["HashKey"] = generateHash($postData); //產生驗證金鑰
	
	$url = "===PAYSERVERAPIURL===";
	$str_Result = formPost($url,$postData);
	echo var_dump($str_Result);
	
	function generateHash($data) {
		$require = array(
			"PlatFormId","OrderId","ReCheckId","Amount","PayTitle","ResURL",
            "PayType","TransType","PeriodNum","CardId","ExpiryDate","CVV","ClientIP",
			"ProductDetail", "Buyer_Identifier","DonateMark","CUSTOMEREMAIL","CarrierId1",
			"NPOBAN" ,"Amount_TaxRate","Reason");

		$paras = array();
		foreach($require as $k=>$v) {
			if(!is_null($v) && $v!="")
			{
				$paras[$v] = ( isset($data[$v]) ? $data[$v] : '' ); 
			}
			
		}
		natcaseksort($paras);
		
		$HashKey = strtoupper(hash('sha256', $data['PlatFormHashKey'].urldecode(http_build_query($paras))));

		return $HashKey;
	}
	
	function formPost($url,$post_data){
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($post_data),
			)
		);

		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;
	}
	function natksort( &$array ) {
		uksort( $array, 'strnatcmp' );
	}
	function natcaseksort( &$array ) {
		uksort( $array, 'strnatcasecmp' );
	}
?>
