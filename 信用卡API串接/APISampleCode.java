import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/APISampleCode")
public class APISampleCode extends HttpServlet {
	private HashMap<String, Integer> TransType = new HashMap<String, Integer>()
	{
	    {
	        put("信用卡一般交易", 1);
	        put("信用卡分期交易", 2);
	        put("信用卡紅利折抵", 3);
	        put("信用卡取消授權", 5);
	        put("信用卡轉入請款檔", 6);
	        put("信用卡轉出請款檔", 7);
	        put("信用卡退貨", 8);
	        put("信用卡取消退貨", 9);
	        put("信用卡交易查詢", 16);
	        put("信用卡結帳", 19);
	        put("銀聯卡授權", 13);
	        put("銀聯卡取消授權", 14);
	        put("銀聯卡取消退貨", 15);
	        put("銀聯卡交易查詢", 17);
	    }
	};
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8"); // 避免PrintWriter寫出中文時出現亂碼
		//get client ip
		String ipAddress="";
	  Set<String> localhostAddresses = new HashSet<String>();
	  localhostAddresses.add(InetAddress.getLocalHost().getHostAddress());
	  for (InetAddress address : InetAddress.getAllByName("localhost")) {
	    localhostAddresses.add(address.getHostAddress());
	  }
	   
	  if (localhostAddresses.contains(request.getRemoteAddr())) {
	   	ipAddress="127.0.0.1";
	  } else {
	   	ipAddress=request.getRemoteAddr();
	  }
		
		//get now time
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		//get timezone
		TimeZone tz = TimeZone.getDefault();  
		Calendar cal = GregorianCalendar.getInstance(tz);
		int offsetInMillis = tz.getOffset(cal.getTimeInMillis());
		String offset = String.format("%02d%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = (offsetInMillis >= 0 ? "+" : "-") + offset;

		//取得特店的HashKey - 由整合支付平台後台取得
        String strPlatFormHashKey = "===HASHKEY===";
        //產生交易資料
        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("PlatFormId", "===MERCHANTID==="); //特店編號 - 必填
        postData.put("PlatFormHashKey", strPlatFormHashKey); //特店HashKey
        postData.put("PayType", "CREDIT"); //支付別 - CREDIT:信用卡, UNION:銀聯卡 - 必填
        postData.put("OrderId", "20991231000001"); //您的訂單編號 - 只允許半形英文字母或數字組合 - 必填
        postData.put("ReCheckId", ""); //整合平台訂單號 - 整合支付平台回傳訂單號，用於退款、查詢之類的API
        postData.put("Amount", "1"); //您的訂單總金額 - 必填
        postData.put("PayTitle", "付款說明"); //付款說明 - 必填
        postData.put("ClientIP", ipAddress); //消費者IP位址 - 必填
        postData.put("ResURL", ""); //回傳網址 - 資料回傳網址(保留欄位) - 必填
        postData.put("HeadquarterID", ""); //集團別 - 由商家自行定義，例如: Grand Holding
        postData.put("StatesID", ""); //區域或州界別 - 由商家自行定義，例如: TW
        postData.put("BranchID", ""); //平台通路或社群別 - 由商家自行定義，例如: ePay
        postData.put("BusinessUnit", ""); //業務單位別 - 由商家自行定義，例如: PCHOME_EC
        postData.put("SubMerchantID", ""); //次特店別 - 由商家自行定義，例如: 12345678
        postData.put("TimeZone", offset); //時區 - 格式：+0800 - 必填
        postData.put("CreateTime", dateFormat.format(date)); //訂單建立時間 - 格式：yyyy/MM/dd HH:mm:ss - 必填
        postData.put("TransTime", dateFormat.format(date)); //發生交易時間 - 格式：yyyy/MM/dd HH:mm:ss - 必填
        postData.put("TransType", TransType.get("信用卡一般交易").toString()); //交易類型 - 格式：yyyy/MM/dd HH:mm:ss - 必填
        postData.put("PeriodNum", ""); //分期交易之期數
        postData.put("CardId", ""); //信用卡卡號 - 必填
        postData.put("ExpiryDate", ""); //信用卡有效日期 - 格式YYMM - 必填
        postData.put("CVV", ""); //信用卡背面末三碼 - 必填
		/*  電子發票專區設定  除商品明細、稅率 皆可透過PayType設為ALL 至整合支付平台支付頁面讓使用者自行輸入*/
		postData.put("ProductDetail",'[{"Description":"紅茶","Quantity":"2","UnitPrice":"15","Amount":"30","Tax":"0","TaxType":"3"},{"Description":"綠茶","Quantity":"5","UnitPrice":"20","Amount":"100","Tax":"0","TaxType":"3"}]');////若有申請電子發票 必填 由各支付欄位產生的JSON字串
		postData.put("Buyer_Identifier","");//買方統編
		postData.put("DonateMark","");//捐贈註記 : 非捐贈發票 1: 捐贈發票
		postData.put("CUSTOMEREMAIL","");//客戶接收電子發票信箱
		postData.put("CarrierId1","");//客戶載具號碼
		postData.put("NPOBAN","");//捐贈發票愛心碼
		postData.put("Amount_TaxRate","");//稅率 5%時本欄位為 0.05
		postData.put("Reason","");//作廢原因
        postData.put("HashKey", sha256(postData)); //產生驗證金鑰
        
        PrintWriter writer = response.getWriter();
        String url = "===PAYSERVERAPIURL===";
        writer.println(formPost(url,postData));
        
	}
	private String formPost(String request, HashMap<String, String> post_data) throws IOException {
		String urlParameters  = http_build_query(post_data);
		byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
		int    postDataLength = postData.length;
		URL    url            = new URL( request );
		HttpURLConnection conn= (HttpURLConnection) url.openConnection();           
		conn.setDoOutput( true );
		conn.setInstanceFollowRedirects( false );
		conn.setRequestMethod( "POST" );
		conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
		conn.setRequestProperty( "charset", "utf-8");
		conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
		conn.setUseCaches( false );
		 // Send POST output.
	    DataOutputStream printout = new DataOutputStream (conn.getOutputStream ());
	    printout.write (postData);
	    printout.flush ();
	    printout.close ();
	    // Get response data.
	    BufferedReader bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	    String line = "";
	    StringWriter writer = new StringWriter();
	    while((line=bufReader.readLine())!=null){
	        writer.write(line);
	    }
	    bufReader.close();
	    writer.close();
	    return writer.toString();
	}
	public String sha256(Map<String, String> data) {
        List<String> required = new ArrayList<String>(){{
            add("PlatFormId");
            add("OrderId");
            add("ReCheckId");
            add("Amount");
            add("PayTitle");
            add("ResURL");
            add("PayType");
            add("TransType");
            add("PeriodNum");
            add("CardId");
            add("ExpiryDate");
            add("CVV");
            add("ClientIP");
			add("ProductDetail");
			add("Buyer_Identifier");
			add("DonateMark");
			add("CUSTOMEREMAIL");
			add("CarrierId1");
			add("NPOBAN");
			add("Amount_TaxRate");
			add("Reason");
        }};
        Collections.sort(required, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        StringBuilder sb = new StringBuilder();
        for(String idx : required) {
			if(data.get(idx) != null && !data.get(idx).isEmpty())
			{
				sb.append("&"+idx+"="+data.get(idx));
			}
				
        }
        String str = sb.toString();
        String base = data.get("PlatFormHashKey")+str.substring(1, str.length());
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
	private String http_build_query(Map<String, String> params) throws UnsupportedEncodingException {
		String internalEncoding = "UTF-8";
	    String result = "";
	    for(Map.Entry<String, String> e : params.entrySet()){
	        if(e.getKey().isEmpty()) continue;
	        if(!result.isEmpty()) result += "&";
	        result += URLEncoder.encode(e.getKey(), internalEncoding) + "=" + 
	                  URLEncoder.encode(e.getValue(), internalEncoding);
	    }
	    return result;
	}
}
